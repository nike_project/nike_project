let items = [
    {
        "img": "src/new-featured-1.jfif",
        "status": "Sold out",
        "name": "Nike Pegasus 41",
        "info": "Men's Road Running Shoes",
        "colors": "9 Colors",
        "price": 140,
        "gender": "man"
    },
    {
        "img": "src/new-featured-2.jfif",
        "status": "Sold out",
        "name": "Nike Pegasus 41",
        "info": "Men's Road Running Shoes",
        "colors": "9 Colors",
        "price": 140,
        "gender": "man"
    },
    {
        "img": "src/new-featured-3.jfif",
        "status": "Just In",
        "name": "Nike Pegasus 41",
        "info": "Men's Road Running Shoes",
        "colors": "9 Colors",
        "price": 140,
        "gender": "man"
    },
    {
        "img": "src/new-featured-4.jfif",
        "status": "Just In",
        "name": "Nike ACG WATERCAT +",
        "info": "Men's Watercat+ Sneakers",
        "colors": "3 Colors",
        "price": 120,
        "gender": "unisex"
    },
    {
        "img": "src/new-featured-5.jfif",
        "status": "Just In",
        "name": "Air Jordan 12 Retro",
        "info": "Big Kids' Shoes",
        "colors": "1 Colors",
        "price": 150,
        "gender": "boy"
    },
    {
        "img": "src/new-featured-6.jfif",
        "status": "Just In",
        "name": "Nike Air Pegasus '89",
        "info": "Golf Shoes",
        "colors": "1 Colors",
        "price": 140,
        "gender": "unisex"
    },
    {
        "img": "src/new-featured-7.jfif",
        "status": "Just In",
        "name": "Nike Dunk Low Retro",
        "info": "Men's Shoes",
        "colors": "3 Colors",
        "price": 115,
        "gender": "man"
    },
    {
        "img": "src/new-featured-8.jfif",
        "status": "Sold out",
        "name": "Nike Air Jordan 1 Retro Low",
        "info": "Lifestyle Shoes",
        "colors": "9 Colors",
        "price": 100,
        "gender": "woman"
    },
    {
        "img": "src/new-featured-9.jfif",
        "status": "Sold out",
        "name": "Air Jordan 1 Retro High OG",
        "info": "Nike's first signature model",
        "colors": "9 Colors",
        "price": 120,
        "gender": "unisex"
    },
    {
        "img": "src/new-featured-10.jfif",
        "status": "Sold out",
        "name": "Air Jordan 4",
        "info": "Men's Shoes",
        "colors": "3 Colors",
        "price": 170,
        "gender": "unisex"
    },
    {
        "img": "src/new-featured-11.jfif",
        "status": "Sold out",
        "name": "Nike",
        "info": "Kids' Shoes",
        "colors": "2 Colors",
        "price": 110,
        "gender": "girl"
    },
    {
        "img": "src/new-featured-12.jfif",
        "status": "Just In",
        "name": "Air Jordan 4",
        "info": "Men's Shoes",
        "colors": "3 Colors",
        "price": 170,
        "gender": "unisex"
    },
    {
        "img": "src/new-featured-13.jfif",
        "status": "Just In",
        "name": "Jordan Spizike Low",
        "info": "Men's Shoes",
        "colors": "3 Colors",
        "price": 125,
        "gender": "man"
    },
    {
        "img": "src/new-featured-14.jfif",
        "status": "Just In",
        "name": "Nike KD17",
        "info": "Basketball Shoes",
        "colors": "3 Colors",
        "price": 250,
        "gender": "man"
    },
    {
        "img": "src/new-featured-15.jfif",
        "status": "Just In",
        "name": "Nike Zoom Vomero 5",
        "info": "Men's Shoes",
        "colors": "2 Colors",
        "price": 200,
        "gender": "man"
    },
    {
        "img": "src/new-featured-16.jfif",
        "status": "Just In",
        "name": "Air Jordan 6 Retro",
        "info": "Men's Shoes",
        "colors": "2 Colors",
        "price": 210,
        "gender": ""
    },
    {
        "img": "src/new-featured-17.jfif",
        "status": "Just In",
        "name": "Air Jordan 6 Retro",
        "info": "Toddler Kids' Shoes",
        "colors": "2 Colors",
        "price": 75,
        "gender": "boy"
    },
    {
        "img": "src/new-featured-18.jfif",
        "status": "Sold out",
        "name": "Air Jordan 6 Retro",
        "info": "Kids' Shoes",
        "colors": "2 Colors",
        "price": 115,
        "gender": "boy"
    },
    {
        "img": "src/new-featured-19.jfif",
        "status": "Sold out",
        "name": "Nike Dunk Low",
        "info": "Lifestyle Shoes",
        "colors": "4 Colors",
        "price": 150,
        "gender": "unisex"
    },
    {
        "img": "src/new-featured-20.jfif",
        "status": "Just In",
        "name": "RTFKT x Nike Dunk Genesis",
        "info": "Men's Shoes",
        "colors": "1 Colors",
        "price": 222,
        "gender": "man"
    },
    {
        "img": "src/new-featured-21.jfif",
        "status": "Just In",
        "name": "Nike Free Metcon 6",
        "info": "Women's Shoes",
        "colors": "1 Colors",
        "price": 180,
        "gender": "Woman"
    },
    {
        "img": "src/new-featured-22.jfif",
        "status": "Sold out",
        "name": "Nike Dunk Low",
        "info": "Lifestyle Shoes",
        "colors": "4 Colors",
        "price": 150,
        "gender": "unisex"
    },
    {
        "img": "src/new-featured-23.jfif",
        "status": "Just In",
        "name": "Nike Aura",
        "info": "Crossbody Bag",
        "colors": "1 Colors",
        "price": 100,
        "gender": "unisex"
    },
    {
        "img": "src/new-featured-24.jfif",
        "status": "Sold out",
        "name": "Air Jordan 11 Retro Low",
        "info": "Women's Shoes",
        "colors": "1 Colors",
        "price": 190,
        "gender": "woman"
    },
    {
        "img": "src/new-featured-25.jfif",
        "status": "Sold out",
        "name": "Air Jordan 1 Retro High",
        "info": "Lifestyle Shoes",
        "colors": "2 Colors",
        "price": 140,
        "gender": "unisex"
    },
    {
        "img": "src/new-featured-26.jfif",
        "status": "Just In",
        "name": "Jordan 1 Shoes Low",
        "info": "Men's Shoes",
        "colors": "2 Colors",
        "price": 140,
        "gender": "man"
    },
    {
        "img": "src/new-featured-27.jfif",
        "status": "Just In",
        "name": "Nike Air DT Max '96",
        "info": "Men's Shoes",
        "colors": "3 Colors",
        "price": 170,
        "gender": "man"
    },
    {
        "img": "src/new-featured-28.jfif",
        "status": "Sold out",
        "name": "Nike Sportswear Chill Knit",
        "info": "Women's Slim Cropped T-Shirt",
        "colors": "3 Colors",
        "price": 40,
        "gender": "woman"
    },
    {
        "img": "src/new-featured-29.jfif",
        "status": "Sold out",
        "name": "Nike Air Dri-FIT",
        "info": "Women's Short-Sleeve Running Top",
        "colors": "9 Colors",
        "price": 80,
        "gender": "woman"
    },
    {
        "img": "src/new-featured-30.jfif",
        "status": "Sold out",
        "name": "Nike SB Force 58",
        "info": "Men's Road Running Shoes",
        "colors": "2 Colors",
        "price": 90,
        "gender": "girl"
    },
    {
        "img": "src/new-featured-31.jfif",
        "status": "Sold out",
        "name": "Sportswear Chill Knit T-Shirt",
        "info": "Women's Running Kit",
        "colors": "2 Colors",
        "price": 50,
        "gender": "woman"
    },
    {
        "img": "src/new-featured-32.jfif",
        "status": "Just In",
        "name": "Naomi Osaka",
        "info": "Cardigan",
        "colors": "1 Color",
        "price": 175,
        "gender": "women"
    },
    {
        "img": "src/new-featured-33.jfif",
        "status": "Just In",
        "name": "Nike Aura",
        "info": "Crossbody Bag",
        "colors": "3 Colors",
        "price": 40,
        "gender": "woman"
    },
    {
        "img": "src/new-featured-34.jfif",
        "status": "Just In",
        "name": "Nike Calm SE",
        "info": "Girl's Flip Flops",
        "colors": "3 Colors",
        "price": 30,
        "gender": "girl"
    },
    {
        "img": "src/new-featured-35.jfif",
        "status": "Sold out",
        "name": "Nike Dri-FIT Club",
        "info": "Structured Swoosh Cap",
        "colors": "2 Colors",
        "price": 20,
        "gender": "unisex"
    },
    {
        "img": "src/new-featured-36.jfif",
        "status": "Sold out",
        "name": "Jordan Club",
        "info": "Unstructured Curved-Bill Hat",
        "colors": "2 Colors",
        "price": 25,
        "gender": "unisex"
    },
    {
        "img": "src/new-featured-37.jfif",
        "status": "Just In",
        "name": "Nike Club",
        "info": "Men's Lined Flow Shorts",
        "colors": "3 Colors",
        "price": 60,
        "gender": "man"
    },
    {
        "img": "src/new-featured-38.jfif",
        "status": "Sold out",
        "name": "NOCTA",
        "info": "CS Nylon Shorts",
        "colors": "9 Colors",
        "price": 70,
        "gender": "unisex"
    },
    {
        "img": "src/new-featured-39.jfif",
        "status": "Just In",
        "name": "NOCTA",
        "info": "Northstar Nylon Tracksuit Jacket",
        "colors": "2 Colors",
        "price": 100,
        "gender": "unisex"
    },
    {
        "img": "src/new-featured-40.jfif",
        "status": "Just In",
        "name": "NOCTA x Nike",
        "info": "Northstar Nylon Tracksuit Jacket",
        "colors": "2 Colors",
        "price": 100,
        "gender": "unisex"
    },
    {
        "img": "src/new-featured-41.jfif",
        "status": "Just In",
        "name": "Nocta",
        "info": "Nocta Fleece CS",
        "colors": "4 Colors",
        "price": 80,
        "gender": "unisex"
    },
    {
        "img": "src/new-featured-42.jfif",
        "status": "Just In",
        "name": "NOCTS Cap",
        "info": "S.S.C Cap",
        "colors": "2 Colors",
        "price": 25,
        "gender": "unisex"
    },
    {
        "img": "src/new-featured-43.jfif",
        "status": "Just In",
        "name": "Nike Phantom Luna 2 Elite",
        "info": "FG High-Top Soccer Cleats",
        "colors": "1 Color",
        "price": 285,
        "gender": "man"
    },
    
]


function generateShoeBlock(item) {
    let shoesBlock = document.createElement("div");
    shoesBlock.className = "shoe-box";
    let img = document.createElement("img");
    img.src = item.img;
    shoesBlock.appendChild(img);
    shoesBlock.innerHTML += `
                <p class="shoes-block-txt-1">${item.status}</p>
                <p class="shoes-block-txt-2">${item.name}</p>
                <p class="shoes-block-txt-3">${item.info}</p>
                <p class="shoes-block-txt-4">${item.colors}</p>
                <p class="shoes-block-txt-5">$${item.price}</p>
    
            `
    return shoesBlock;
}

let wrapper = document.querySelector(".new-featured-shoes-wrapper");
let releaseCnt = document.querySelector(".release-cnt");
let cnt = 0


function itemDisplay() {
    cnt = 0;
    let wrapper = document.querySelector(".new-featured-shoes-wrapper");
    wrapper.innerHTML = "";
    for (let item of items) {
        wrapper.appendChild(generateShoeBlock(item));
    }
    document.querySelector(".release-cnt").innerHTML = items.length
}

itemDisplay()


function menSelect() {
    cnt = 0;
    wrapper.innerHTML = "";
    for (let item of items) {
        if (item.gender === "man" || item.gender === "unisex") {
            wrapper.appendChild(generateShoeBlock(item));
            cnt++;
        }
    }
    releaseCnt.innerHTML = cnt;
}

function womenSelect() {
    cnt = 0;
    wrapper.innerHTML = "";
    for (let item of items) {
        if (item.gender === "woman" || item.gender === "unisex") {
            wrapper.appendChild(generateShoeBlock(item));
            cnt++;
        }
    }
    releaseCnt.innerHTML = cnt;
}

function universalSelect() {
    cnt = 0;
    wrapper.innerHTML = "";
    for (let item of items) {
        if (item.gender === "unisex") {
            wrapper.appendChild(generateShoeBlock(item));
            cnt++;
        }
    }
    releaseCnt.innerHTML = cnt;
}

function kidsBoysSelect() {
    cnt = 0;
    wrapper.innerHTML = "";
    for (let item of items) {
        if (item.gender === "boy") {
            wrapper.appendChild(generateShoeBlock(item));
            cnt++;
        }
    }
    releaseCnt.innerHTML = cnt;
}

function kidsGirlsSelect() {
    cnt = 0;
    wrapper.innerHTML = "";
    for (let item of items) {
        if (item.gender === "girl") {
            wrapper.appendChild(generateShoeBlock(item));
            cnt++;
        }
    }
    releaseCnt.innerHTML = cnt;
}

document.querySelector(".man-gender").addEventListener("change", (e) => e.target.checked ? menSelect() : 0);
document.querySelector(".woman-gender").addEventListener("change", (e) => e.target.checked ? womenSelect() : 0);
document.querySelector(".unisex-gender").addEventListener("change", (e) => e.target.checked ? universalSelect() : 0);
document.querySelector(".all-gender").addEventListener("change", (e) => e.target.checked ? itemDisplay() : 0);
document.querySelector(".kids-boys").addEventListener("change", (e) => e.target.checked ? kidsBoysSelect() : 0);
document.querySelector(".kids-girls").addEventListener("change", (e) => e.target.checked ? kidsGirlsSelect() : 0);

let prices = document.querySelectorAll(".prices");
prices.forEach((price) => {
    price.addEventListener("change", () => {
        let cnt = 0;
        wrapper.innerHTML = "";
        for (let item of items) {
            if (
                (price.value === "over" && item.price > 150) ||
                (price.value === "25" && item.price >= 0 && item.price <= 25) ||
                (price.value === "50" && item.price > 25 && item.price <= 50) ||
                (price.value === "100" && item.price > 50 && item.price <= 100) ||
                (price.value === "150" && item.price > 100 && item.price <= 150)
            ) {
                wrapper.appendChild(generateShoeBlock(item));
                cnt++;
            }
        }
        releaseCnt.innerHTML = cnt

    });
});

let brands = document.querySelectorAll(".brands");
brands.forEach((brand) => {
    brand.addEventListener("change", () => {
        let cnt = 0;
        wrapper.innerHTML = "";
        for (let item of items) {
            if (
                (brand.value === "sportswear" && item.brand === "sportswear") ||
                (brand.value === "jordan" && item.brand === "jordan") ||
                (brand.value === "nike" && item.brand === "nike") ||
                (brand.value === "converse" && item.brand === "converse") ||
                (brand.value === "nikelab" && item.brand === "nikelab") ||
                (brand.value === "acg" && item.brand === "acg")
            ) {
                wrapper.appendChild(generateShoeBlock(item));
                cnt++;
            }
        }
        releaseCnt.innerHTML = cnt;

    });
});

let activities = document.querySelectorAll(".activity");
activities.forEach((activity) => {
    activity.addEventListener("change", () => {
        let cnt = 0;
        wrapper.innerHTML = "";
        for (let item of items) {
            if (
                (activity.value === "lifestyle" && item.activity === "lifestyle") ||
                (activity.value === "running" && item.activity === "running") ||
                (activity.value === "baketball" && item.activity === "baketball") ||
                (activity.value === "training" && item.activity === "training") ||
                (activity.value === "walking" && item.activity === "walking") ||
                (activity.value === "hiking" && item.activity === "hiking")
            ) {
                wrapper.appendChild(generateShoeBlock(item));
                cnt++;
            }
        }
        releaseCnt.innerHTML = cnt;

    });
});

let bestFor = document.querySelectorAll(".best-for");
bestFor.forEach((best) => {
    best.addEventListener("change", () => {
        let cnt = 0;
        wrapper.innerHTML = "";
        for (let item of items) {
            if (
                (best.value === "wet" && item.bestFor === "wet") ||
                (best.value === "cold" && item.bestFor === "cold") ||
                (best.value === "dry" && item.bestFor === "dry")
            ) {
                wrapper.appendChild(generateShoeBlock(item));
                cnt++;
            }
        }
        releaseCnt.innerHTML = cnt;

    });
});