// header scroll
let lastScrollTop = 0;
let header = document.querySelector("header");
window.addEventListener("scroll", () => {
    let scrollTop = document.documentElement.scrollTop;
    if (scrollTop > lastScrollTop) {
        header.style.top = "-80px";
    } else {
        header.style.top = "-1px";
    }

    lastScrollTop = scrollTop;
})

// expand side search block
let searchBlock = document.querySelector(".search-box");
let sideSearchBlock = document.querySelector(".search-expand-box");
let cancelBtn = document.querySelector(".cancel-btn");

searchBlock.addEventListener("click", () => {
    sideSearchBlock.classList.add("active-search")
    overlay.style.display = "initial";
    document.body.style.height = "100vh";
    document.body.style.overflow = "hidden";
})

cancelBtn.addEventListener("click", () => {
    sideSearchBlock.classList.remove("active-search");
    overlay.style.display = "none";
    document.body.style.height = "auto";
    document.body.style.overflow = "hidden";
})

// side menu
let menuIcon = document.querySelector(".icon-menu");
let sideMenu = document.querySelector(".side-menu");
let overlay = document.querySelector(".overlay");
let exitBtn = document.querySelector(".side-menu-exit-icon");

menuIcon.addEventListener("click", () => {
    sideMenu.classList.add("side-menu-active");
    overlay.style.display = "initial";
    document.body.style.height = "100vh";
    document.body.style.overflow = "hidden";
})

overlay.addEventListener("click", () => {
    sideSearchBlock.classList.remove("active-search")
    sideMenu.classList.remove("side-menu-active");
    overlay.style.display = "none";
    document.body.style.height = "auto";
    document.body.style.overflow = "";
})

exitBtn.addEventListener("click", () => {
    sideMenu.classList.remove("side-menu-active");
    overlay.style.display = "none";
    document.body.style.height = "auto";
    document.body.style.overflow = "";
})

// nav links hover
let links = document.querySelectorAll(".hover-link");
let blocks = document.querySelectorAll(".nav-block");

links.forEach((link, index) => {
    link.addEventListener("mouseover", () => {
        blocks[index].style.display = "initial";
        blocks[index].style.height = "auto";
        overlay.style.display = "initial"
    })

    blocks[index].addEventListener("mouseover", () => {
        blocks[index].style.display = "initial";
        blocks[index].style.height = "auto";
        overlay.style.display = "initial"
    })

    link.addEventListener("mouseout", () => {
        blocks[index].style.display = "none";
        blocks[index].style.height = 0;
        overlay.style.display = "none"
    })

    blocks[index].addEventListener("mouseout", () => {
        blocks[index].style.display = "none";
        blocks[index].style.height = 0;
        overlay.style.display = "none"
    })
})

// footer small screen
let footerBlocks = document.querySelectorAll(".footer-extra-links");
let footerLinks = document.querySelectorAll(".extra-link-ul");
let arrowIcon = document.querySelectorAll(".bx-chevron-up");

footerLinks.forEach((link, index) => {
    let click = false;

    link.addEventListener("click", () => {
        click = !click;
        if (click === true) {
            footerBlocks[index].style.height = "300px";
            arrowIcon[index].style.transform = `rotate(${180}deg)`;
        } else {
            footerBlocks[index].style.height = "0px";
            arrowIcon[index].style.transform = `rotate(${0}deg)`;
        }
    })
})


// sort and filter buttons
let filterBtn = document.querySelector(".filter-btn");
let sortBtn = document.querySelector(".sort-btn");
let mainFeaturedBlock = document.querySelector(".new-featured-shoes-main-wrapper");
let filterBlock = document.querySelector(".new-featured-shoes-filter-wrapper")
let fileterClick = false;
filterBtn.addEventListener("click", () => {
    fileterClick = !fileterClick;
    
    if (fileterClick === true) {
        mainFeaturedBlock.style.gridTemplateColumns = "15% 82%";
        mainFeaturedBlock.style.gap = "50px";
        filterBlock.style.overflowY = "auto"
        filterBlock.style.visibility = "visible"
        filterBtn.innerHTML = `Hide Filters <img src="src/filter.png" alt="">`
    } else {
        mainFeaturedBlock.style.gridTemplateColumns = "0% 100%";
        mainFeaturedBlock.style.gap = "0";
        filterBlock.style.overflowY = "hidden";
        filterBlock.style.visibility = "hidden"
        filterBtn.innerHTML = `Show Filters <img src="src/filter.png" alt="">`
    }
})

// filters nav links
let filterNavBlock = document.querySelectorAll(".filter-nav-expand-blocks");
let filterNavUlLinks = document.querySelectorAll(".filter-nav-ul");
let navArrow = document.querySelectorAll(".nav-arrow")

filterNavUlLinks.forEach((link, index) => {
    let click = false;

    link.addEventListener("click", () => {
        click = !click;
        if (click === true) {
            filterNavBlock[index].style.height = "230px";
            navArrow[index].style.transform = `rotate(${180}deg)`;
        } else {
            filterNavBlock[index].style.height = "40px";
            navArrow[index].style.transform = `rotate(${0}deg)`
        }
    })
})

